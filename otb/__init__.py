# -*- coding: utf-8 -*-
"""
/***************************************************************************
 OTBAlgorithm
                                 A QGIS plugin
 this plugin allow one to use otb through qgis processing
                              -------------------
        begin                : 2018-01-30
        copyright            : (C) 2018 by CS
        email                : rkm
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 This script initializes the plugin, making it known to QGIS.
"""

__author__ = 'CS'
__date__ = '2018-01-30'
__copyright__ = '(C) 2018 by CS'


# noinspection PyPep8Naming
def classFactory(iface):  # pylint: disable=invalid-name
    """Load OTBAlgorithm class from file OTBAlgorithm.

    :param iface: A QGIS interface instance.
    :type iface: QgsInterface
    """
    #
    from .OTBPlugin import OTBPlugin
    return OTBPlugin()
